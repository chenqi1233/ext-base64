# base64

Base64 常用于表示、传输、存储二进制数据，也可以用于将一些含有特殊字符的文本内容编码，以便传输。

![](./python/_images/featured.png)

# 积木

![](./python/_images/block.png)

# 程序实例


![](./python/_images/explame.png)


运行结果，搭配Mind+可视化平台，选择网络图片组件：

![](./python/_images/explame1.png)

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|python|备注|
|-----|-----|-----|-----|:-----:|-----|
|uno|||||||
|micro:bit|||||||
|mpython|||||||
|arduinonano|||||||
|leonardo|||||||
|mega2560|||||||
|行空板||||√|||

# 更新日志

V0.0.1 基础功能完成

V0.0.2 主要结合CV2使用

V0.0.3 新增对图片的积木
